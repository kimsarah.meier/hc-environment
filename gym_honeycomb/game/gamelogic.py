import numpy as np


class HoneyCombGame:
    # Possible actions on gamefield
    UP = (-1, 0)
    DOWN = (+1, 0)
    UP_LEFT = (-1, -1)
    UP_RIGHT = (0, +1)
    DOWN_LEFT = (0, -1)
    DOWN_RIGHT = (+1, +1)
    PASS = (0, 0)

    ACTIONS = [UP, DOWN_RIGHT, DOWN_LEFT, DOWN, UP_LEFT, UP_RIGHT, PASS]
    ACTIONS_NAME = ['UP', 'DOWN_RIGHT', 'DOWN_LEFT', 'DOWN', 'UP_LEFT', 'UP_RIGHT', 'PASS']

    # Tokens
    # Field (NORMAL oder PAYOFF) or not part of the gamefield (VOID)
    TOKEN_VOID = -2
    TOKEN_NORMAL = -1
    TOKEN_PAYOFF = -3

    # Matrix size including void
    GAMEFIELD_SIZE = 13
    PAYOFF_FIELD = [(0, 3), (3, 0), (3, 9), (9, 3), (9, 12), (12, 9)]
    PAYOFF_SPECIAL = [(9, 3)]

    # NUMBER of moves
    MOVES = 15

    def __init__(self, num_agents=10):
        self.gamefield = None
        # available fields on gamefield
        self.fields = None
        # number of agents
        self.agents = num_agents

        # current state (updated every turn)
        # overall moves
        self.turns_count = None
        # individual counter for each agent
        self.remaining_moves = None
        # Current agent interacting with environment
        self.current_agent = None
        self.agent_list = []
        # List with tuples of the current position of each agent
        self.agent_positions = None

        # Episode counter, start with 0
        self.episode = 0

        # Save agents positions for in the course of the game
        self.storage = None

    def reset_game(self):
        """
        Reset the parameters of the honeycomb gamefield.
        :return: -
        """
        # Start with a new gamefield
        self.gamefield = self.new_gamefield()
        # Fields available to access by agents: normal field + payoff field
        self.fields = self.find_token_coord(self.gamefield, self.TOKEN_NORMAL) + self.PAYOFF_FIELD

        # Fill the gamefield with agents: Number corresponds to amount of agents
        # Start at (0,0): gamefield[6,6]
        self.gamefield[6, 6] = 9

        # Reset agents positions
        self.agent_positions = [(6, 6)] * self.agents
        self.storage = [[(6, 6)] for _ in range(self.agents)]

        # Restart game counters
        self.turns_count = 1
        self.remaining_moves = [self.MOVES] * self.agents
        self.current_agent = 0

        # Update episode counter
        self.episode += 1

    # =========================================================================
    #                           The gamefield
    # =========================================================================

    @staticmethod
    def new_gamefield():
        """ Create a new gamefield without agent positions
        :return: a numpy.array: the new gamefield
        """
        """
        # Gamefield as a matrix: (column, row)
        #
        #                             (-3,-6),
        #           (-5,-5), (-4,-5), (-3,-5), (-2,-5), (-1,-5), (0,-5),
        #           (-5,-4), (-4,-4), (-3,-4), (-2,-4), (-1,-4), (0,-4), (1,-4),
        # (-6, -3), (-5,-3), (-4,-3), (-3,-3), (-2,-3), (-1,-3), (0,-3), (1,-3), (2,-3), (3,-3),
        #           (-5,-2), (-4,-2), (-3,-2), (-2,-2), (-1,-2), (0,-2), (1,-2), (2,-2), (3,-2),
        #           (-5,-1), (-4,-1), (-3,-1), (-2,-1), (-1,-1), (0,-1), (1,-1), (2,-1), (3,-1), (4,-1)
        #           (-5, 0), (-4, 0), (-3, 0), (-2, 0), (-1, 0), (0, 0), (1, 0), (2, 0), (3, 0), (4, 0), (5, 0),
        #                    (-4, 1), (-3, 1), (-2, 1), (-1, 1), (0, 1), (1, 1), (2, 1), (3, 1), (4, 1), (5, 1),
        #                             (-3, 2), (-2, 2), (-1, 2), (0, 2), (1, 2), (2, 2), (3, 2), (4, 2), (5, 2),
        #                             (-3, 3), (-2, 3), (-1, 3), (0, 3), (1, 3), (2, 3), (3, 3), (4, 3), (5, 3), (6, 3),
        #                                               (-1, 4), (0, 4), (1, 4), (2, 4), (3, 4), (4, 4), (5, 4),
        #                                                        (0, 5), (1, 5), (2, 5), (3, 5), (4, 5), (5, 5),
        #                                                                                (3, 6)
        #
        """
        n = HoneyCombGame.GAMEFIELD_SIZE
        gamefield = np.full((n, n), HoneyCombGame.TOKEN_NORMAL, dtype=np.int8)
        # set border to void
        gamefield[0, :] = gamefield[-1, :] = gamefield[:, 0] = gamefield[:, -1] = HoneyCombGame.TOKEN_VOID
        for i in range(5):
            gamefield[7 + i, 1:i + 2] = gamefield[i + 1, 7 + i:] = HoneyCombGame.TOKEN_VOID
        # include payoff fields
        gamefield[0, 3] = gamefield[3, 0] = gamefield[3, 9] = gamefield[9, 3] = gamefield[12, 9] = gamefield[9, 12] \
            = HoneyCombGame.TOKEN_PAYOFF
        return gamefield

    def update_gamefield(self):
        """
        Performs an update of the current state of the gamefield. Updates self.gamefield invisibly.
        :return: -
        """
        self.gamefield = self.new_gamefield()
        for coord in self.agent_positions:
            r, c = coord
            self.gamefield[r, c] = self.agent_positions.index(coord)

    def movable_agent(self):
        """
        Determines all agents with remaining moves.
        :return: A list.
        """
        a_list = []
        for a in range(self.agents):
            if self.remaining_moves[a] > 0:
                a_list.append(a)

        return a_list

    def next_agent(self):
        """
        Returns the agent for next step. Only agents with remaining moves are considered.
        :return: A number (0-9)
        """
        next_a = None

        # Create a list of agents with remaining moves
        a_list = self.movable_agent()

        # No agents have available moves left, env is done!
        if len(a_list) == 0:
            env_done = True
        else:
            env_done = False

            if self.current_agent == 9:
                next_a = 0
            else:
                next_a = self.current_agent + 1

            while next_a not in a_list:
                next_a += 1
                if next_a == 10:
                    next_a = 0

        return next_a, env_done

    # =========================================================================
    #                           additional methods
    # =========================================================================
    @staticmethod
    def find_token_coord(gamefield, token):
        """
        Find all the token's position in the given gamefield
        :param gamefield: The gamefield
        :param token: The token to search for
        :return: A list with token positions (tuple)
        """
        n = HoneyCombGame.GAMEFIELD_SIZE
        return [(r, c) for r in range(n) for c in range(n) if gamefield[r, c] == token]

    def accessible_fields(self, coord):
        """
        Calculates accessible fields for current agent position.
        :param coord: Agent position (tuple)
        :return: A list with accessible fields coordinates (tuple)
        """
        r, c = coord
        # Reachable fields from current position for all actions
        f = [(r + dr, c + dc) for (dr, dc) in HoneyCombGame.ACTIONS]
        acc_fields = []
        # Checks is reachable field is a normal or payoff field (or void)
        for i in f:
            acc_fields.append(i) if i in self.fields else acc_fields
        return acc_fields

    def future_actions(self):
        """
        Returns possible move of current agent.
        :return: A list.
        """
        r, c = self.agent_positions[self.current_agent]
        # Reachable fields from current position for all actions
        f = [(r + dr, c + dc) for (dr, dc) in self.ACTIONS]
        possible_action = []
        # Checks is reachable field is a normal or payoff field (or void)
        for i in range(7):
            if f[i] in self.fields:
                possible_action.append(1)
            else:
                possible_action.append(0)
        assert len(possible_action) == 7
        return possible_action

    def check_action(self, new):
        """
        Checks if the agent is allowed to move with given action.
        :param new: New position of agent
        :return: A bool.
        """
        # New position in accessible fields by agent
        if new in self.accessible_fields(self.agent_positions[self.current_agent]):
            return True
        else:
            return False