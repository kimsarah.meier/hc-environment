import pandas as pd
import numpy as np
import gym
import random
from stable_baselines import PPO2
from stable_baselines.common import make_vec_env


class Simulation:
    """
    A class to simulate the HoneyComb Experiment with trained policies.
    """

    def __init__(self):
        self.df_check = pd.DataFrame()
        self.df_move = pd.DataFrame()
        self.df_eval = pd.DataFrame()
        self.total_reward_sum = []
        self.average_reward = 0
        self.agent0 = None
        self.agent1 = None
        self.agent2 = None
        self.agent3 = None
        self.agent4 = None
        self.agent5 = None
        self.agent6 = None
        self.agent7 = None
        self.agent8 = None
        self.agent9 = None
        self.env = None

    def one_agent_simulation(self, agent0, num_sim=200, num_step=600, deterministic=False, verbose=True):
        """
        Simulation for environment with one policy for all agents.
        :param agent0: A string with directory info for PPO2
        :param num_sim: Number of simulations to run
        :param num_step: Maximal number of steps in environment
        :param deterministic: Deterministic or stochastic policy
        :param verbose: Print results
        :return: Three pd.DataFrames with information for evaluation
        """
        if verbose:
            print('\n ------------ Start Simulation ------------')

        # Load agents
        print('Load agent...')
        self.agent0 = PPO2.load(agent0)

        if verbose:
            print('\n ------ Start %s Simulations ------' % num_sim)

        for sim in range(num_sim):
            self.env = gym.make('gym_honeycomb:honeycomb-sim-v0')
            obs = self.env.reset()

            for step in range(num_step):

                action, _states = self.agent0.predict(obs, deterministic=deterministic)
                obs, reward, done, info = self.env.step(action)

                if done:
                    move = self.env.df_move
                    r = [sim] * move.shape[0]
                    move.insert(loc=0, column='sim', value=r)

                    run = [sim] * 10
                    storage = self.env.df_check
                    storage.insert(loc=0, column='sim', value=run)
                    data_eval = self.env.df_eval
                    data_eval.insert(loc=0, column='sim', value=run)

                    self.total_reward_sum.append(reward)
                    self.average_reward = np.array(self.total_reward_sum).mean()
                    if sim % 100 == 0:
                        if verbose:
                            print('%s Games over! Average Reward= %s' % (sim, self.average_reward))

                    break

            self.df_move = self.df_move.append(move)
            self.df_check = self.df_check.append(storage)
            self.df_eval = self.df_eval.append(data_eval)

        # Reset index
        self.df_move.reset_index(drop=True, inplace=True)
        self.df_check.reset_index(drop=True, inplace=True)
        self.df_eval.reset_index(drop=True, inplace=True)

        self.df_eval['moves'] = [15] * self.df_eval.shape[0]
        self.df_eval['moves'] = self.df_eval['moves'] - self.df_eval['remaining moves']
        self.df_check.insert(loc=1, column='agent', value=list(range(10)) * num_sim)
        self.average_reward = np.array(self.total_reward_sum).mean()

        if verbose:
            print('------------ End Simulation ------------\n')
            # Print average reward of simulation
            print('Average Simulation Reward: %s \n' % self.average_reward)
            print('-------------------------------------------\n')

        return self.df_move, self.df_eval, self.df_check

    def multi_agent_simulation(self, agents, num_sim=200, num_step=600, shuffle=False, deterministic=False,
                               verbose=True):
        """
        Simulation for environment with multiple policies: multi-agent RL.
        :param shuffle: Assign policies by random to agent.
        :param agents: A list with directory info for PPO2
        :param num_sim: Number of simulations to run
        :param num_step: Maximal number of steps in environment
        :param deterministic: Deterministic or stochastic policy
        :param verbose: Print results
        :return: Three pd.DataFrames with information for evaluation
        """
        if verbose:
            print('\n ------------ Start MA Simulation ------------')

        # Load agents
        self.load_agents(agents)

        if verbose:
            print('\n ------ Start %s Simulations ------' % num_sim)

        for sim in range(num_sim):
            self.env = gym.make('gym_honeycomb:honeycomb-sim-v0')
            obs = self.env.reset()
            list_agents_maj = [self.agent0, self.agent1, self.agent2, self.agent3, self.agent4, self.agent5,
                               self.agent6, self.agent7]
            list_agents_min = [self.agent8, self.agent9]
            if shuffle:
                maj_order = random.sample(list_agents_maj, k=8)
                min_order = random.sample(list_agents_min, k=2)
                agent_order = maj_order + min_order
            else:
                agent_order = list_agents_maj + list_agents_min

            agent0 = agent_order[0]
            agent1 = agent_order[1]
            agent2 = agent_order[2]
            agent3 = agent_order[3]
            agent4 = agent_order[4]
            agent5 = agent_order[5]
            agent6 = agent_order[6]
            agent7 = agent_order[7]
            agent8 = agent_order[8]
            agent9 = agent_order[9]

            for step in range(num_step):

                if self.env.agent == 0:
                    action, _states = agent0.predict(obs, deterministic=deterministic)

                elif self.env.agent == 1:
                    action, _states = agent1.predict(obs, deterministic=deterministic)

                elif self.env.agent == 2:
                    action, _states = agent2.predict(obs, deterministic=deterministic)

                elif self.env.agent == 3:
                    action, _states = agent3.predict(obs, deterministic=deterministic)

                elif self.env.agent == 4:
                    action, _states = agent4.predict(obs, deterministic=deterministic)

                elif self.env.agent == 5:
                    action, _states = agent5.predict(obs, deterministic=deterministic)

                elif self.env.agent == 6:
                    action, _states = agent6.predict(obs, deterministic=deterministic)

                elif self.env.agent == 7:
                    action, _states = agent7.predict(obs, deterministic=deterministic)

                elif self.env.agent == 8:
                    action, _states = agent8.predict(obs, deterministic=deterministic)

                elif self.env.agent == 9:
                    action, _states = agent9.predict(obs, deterministic=deterministic)

                obs, reward, done, info = self.env.step(action)

                if done:
                    move = self.env.df_move
                    r = [sim] * move.shape[0]
                    move.insert(loc=0, column='sim', value=r)

                    run = [sim] * 10
                    storage = self.env.df_check
                    storage.insert(loc=0, column='sim', value=run)
                    data_eval = self.env.df_eval
                    data_eval.insert(loc=0, column='sim', value=run)

                    self.total_reward_sum.append(reward)
                    self.average_reward = np.array(self.total_reward_sum).mean()
                    if sim % 100 == 0:
                        if verbose:
                            print('%s Games over! Average Reward= %s' % (sim, self.average_reward))

                    break

            self.df_move = self.df_move.append(move)
            self.df_check = self.df_check.append(storage)
            self.df_eval = self.df_eval.append(data_eval)

        # Reset index
        self.df_move.reset_index(drop=True, inplace=True)
        self.df_check.reset_index(drop=True, inplace=True)
        self.df_eval.reset_index(drop=True, inplace=True)

        self.df_eval['moves'] = [15] * self.df_eval.shape[0]
        self.df_eval['moves'] = self.df_eval['moves'] - self.df_eval['remaining moves']
        self.df_check.insert(loc=1, column='agent', value=list(range(10)) * num_sim)
        self.average_reward = np.array(self.total_reward_sum).mean()

        if verbose:
            print('------------ End MA Simulation ------------\n')
            # Print average reward of simulation
            print('Average Simulation Reward: %s \n' % self.average_reward)
            print('-------------------------------------------\n')

        return self.df_move, self.df_eval, self.df_check

    def load_agents(self, directory_list):
        """ Load policies for agents to play the game.
        :param directory_list: A list containig directory info for each agent
        :return: Nothing
        """
        no = len(directory_list)
        if no != 10:
            print('Not enough agents!')
        else:
            print('Load agents...')
            self.agent0 = PPO2.load(directory_list[0])
            self.agent1 = PPO2.load(directory_list[1])
            self.agent2 = PPO2.load(directory_list[2])
            self.agent3 = PPO2.load(directory_list[3])
            self.agent4 = PPO2.load(directory_list[4])
            self.agent5 = PPO2.load(directory_list[5])
            self.agent6 = PPO2.load(directory_list[6])
            self.agent7 = PPO2.load(directory_list[7])
            self.agent8 = PPO2.load(directory_list[8])
            self.agent9 = PPO2.load(directory_list[9])

    @property
    def check_end_position(self):
        if len(self.df_check) == 0:
            print('The simulation has not been run yet.')
        else:
            return self.df_check.groupby('end_position').count()

    @property
    def eval_summary(self):
        if len(self.df_eval) == 0:
            print('The simulation has not been run yet.')
        else:
            return self.df_eval.describe()

    def save_results(self, path):
        self.df_move.to_csv(path + '/sim_train_move.csv', index=True)
        self.df_check.to_csv(path + '/sim_train_check.csv', index=True)
        self.df_eval.to_csv(path + '/sim_train_eval.csv', index=True)

        print('Saved dataframes in folder:', path)

    @property
    def sim_reward_mean(self):
        return np.array(self.total_reward_sum).mean()

    @property
    def env_info(self):
        return print('The simulation environment for HoneyComb is used: honeycomb-sim-v0.')


class DummySimulation(Simulation):
    """
    Simulation for vectorized environments.
    """

    def __init__(self):
        super().__init__()

    def one_agent_simulation(self, agent0, num_sim=200, num_step=600, deterministic=False, verbose=True):
        """
        Simulation for environment with one policy for all agents.
        :param agent0: A string with directory info for PPO2
        :param num_sim: Number of simulations to run
        :param num_step: Maximal number of steps in environment
        :param deterministic: deterministic or stochastic policy
        :param verbose: Print results
        :return: Three pd.DataFrames with information for evaluation
        """
        if verbose:
            print('\n ------------ Start Simulation ------------')

        # Load agents
        print('Load agent...')
        self.agent0 = PPO2.load(agent0)

        if verbose:
            print('\n ------ Start %s Simulations ------' % num_sim)

        for sim in range(num_sim):
            self.env = make_vec_env('gym_honeycomb:honeycomb-sim-v0', 1)
            obs = self.env.reset()

            for step in range(num_step):

                action, _states = self.agent0.predict(obs, deterministic=deterministic)
                obs, reward, done, info = self.env.step(action)

                if done[0]:
                    move = self.env.get_attr('df_move', 0)[0]
                    r = [sim] * move.shape[0]
                    move.insert(loc=0, column='sim', value=r)

                    run = [sim] * 10
                    storage = self.env.get_attr('df_check')[0]
                    storage.insert(loc=0, column='sim', value=run)
                    data_eval = self.env.get_attr('df_eval')[0]
                    data_eval.insert(loc=0, column='sim', value=run)

                    self.total_reward_sum.append(reward[0])
                    self.average_reward = np.array(self.total_reward_sum).mean()
                    if sim % 100 == 0:
                        if verbose:
                            print('%s Games over! Average Reward= %s' % (sim, self.average_reward))

                    break

            self.df_move = self.df_move.append(move)
            self.df_check = self.df_check.append(storage)
            self.df_eval = self.df_eval.append(data_eval)

        # Reset index
        self.df_move.reset_index(drop=True, inplace=True)
        self.df_check.reset_index(drop=True, inplace=True)
        self.df_eval.reset_index(drop=True, inplace=True)

        self.df_eval['moves'] = [15] * self.df_eval.shape[0]
        self.df_eval['moves'] = self.df_eval['moves'] - self.df_eval['remaining moves']
        self.df_check.insert(loc=1, column='agent', value=list(range(10)) * num_sim)
        self.average_reward = np.array(self.total_reward_sum).mean()

        if verbose:
            print('------------ End Simulation ------------\n')
            # Print average reward of simulation
            print('Average Simulation Reward: %s \n' % self.average_reward)
            print('-------------------------------------------\n')

        return self.df_move, self.df_eval, self.df_check

    def multi_agent_simulation(self, agents, num_sim=200, num_step=600, shuffle=False, deterministic=False,
                               verbose=True):
        """
        Simulation for environment with multiple policies: multi-agent RL.
        :param shuffle: Assign policies randomly to agent.
        :param agents: A list with directory info for PPO2
        :param num_sim: Number of simulations to run
        :param num_step: Maximal number of steps in environment
        :param deterministic: Deterministic or stochastic policy
        :param verbose: Print results
        :return: Three pd.DataFrames with information for evaluation
        """
        if verbose:
            print('\n ------------ Start MA Simulation ------------')

        # Load agents
        self.load_agents(agents)

        if verbose:
            print('\n ------ Start %s Simulations ------' % num_sim)

        for sim in range(num_sim):
            self.env = gym.make('gym_honeycomb:honeycomb-sim-v0')
            obs = self.env.reset()
            list_agents_maj = [self.agent0, self.agent1, self.agent2, self.agent3, self.agent4, self.agent5,
                               self.agent6, self.agent7]
            list_agents_min = [self.agent8, self.agent9]
            if shuffle:
                maj_order = random.sample(list_agents_maj, k=8)
                min_order = random.sample(list_agents_min, k=2)
                agent_order = maj_order + min_order
            else:
                agent_order = list_agents_maj + list_agents_min

            agent0 = agent_order[0]
            agent1 = agent_order[1]
            agent2 = agent_order[2]
            agent3 = agent_order[3]
            agent4 = agent_order[4]
            agent5 = agent_order[5]
            agent6 = agent_order[6]
            agent7 = agent_order[7]
            agent8 = agent_order[8]
            agent9 = agent_order[9]

            for step in range(num_step):

                if self.env.agent == 0:
                    action, _states = agent0.predict(obs, deterministic=deterministic)

                elif self.env.agent == 1:
                    action, _states = agent1.predict(obs, deterministic=deterministic)

                elif self.env.agent == 2:
                    action, _states = agent2.predict(obs, deterministic=deterministic)

                elif self.env.agent == 3:
                    action, _states = agent3.predict(obs, deterministic=deterministic)

                elif self.env.agent == 4:
                    action, _states = agent4.predict(obs, deterministic=deterministic)

                elif self.env.agent == 5:
                    action, _states = agent5.predict(obs, deterministic=deterministic)

                elif self.env.agent == 6:
                    action, _states = agent6.predict(obs, deterministic=deterministic)

                elif self.env.agent == 7:
                    action, _states = agent7.predict(obs, deterministic=deterministic)

                elif self.env.agent == 8:
                    action, _states = agent8.predict(obs, deterministic=deterministic)

                elif self.env.agent == 9:
                    action, _states = agent9.predict(obs, deterministic=deterministic)

                obs, reward, done, info = self.env.step(action)

                if done[0]:
                    move = self.env.get_attr('df_move', 0)[0]
                    r = [sim] * move.shape[0]
                    move.insert(loc=0, column='sim', value=r)

                    run = [sim] * 10
                    storage = self.env.get_attr('df_check')[0]
                    storage.insert(loc=0, column='sim', value=run)
                    data_eval = self.env.get_attr('df_eval')[0]
                    data_eval.insert(loc=0, column='sim', value=run)

                    self.total_reward_sum.append(reward[0])
                    self.average_reward = np.array(self.total_reward_sum).mean()
                    if sim % 100 == 0:
                        if verbose:
                            print('%s Games over! Average Reward= %s' % (sim, self.average_reward))

                    break

            self.df_move = self.df_move.append(move)
            self.df_check = self.df_check.append(storage)
            self.df_eval = self.df_eval.append(data_eval)

        # Reset index
        self.df_move.reset_index(drop=True, inplace=True)
        self.df_check.reset_index(drop=True, inplace=True)
        self.df_eval.reset_index(drop=True, inplace=True)

        self.df_eval['moves'] = [15] * self.df_eval.shape[0]
        self.df_eval['moves'] = self.df_eval['moves'] - self.df_eval['remaining moves']
        self.df_check.insert(loc=1, column='agent', value=list(range(10)) * num_sim)

        if verbose:
            print('------------ End MA Simulation ------------\n')
            # Print average reward of simulation
            print('Average Simulation Reward: %s \n' % self.average_reward)
            print('-------------------------------------------\n')

        return self.df_move, self.df_eval, self.df_check