import numpy as np
import pandas as pd
import collections

mapmap = pd.DataFrame([[-4.5, 2.5980800, -6, -3],
                       [-5, 0.0000000, -5, 0],
                       [-4.5, 0.8660250, -5, -1],
                       [-4, 1.7320000, -5, -2],
                       [-3.5, 2.5980800, -5, -3],
                       [-3, 3.4641000, -5, -4],
                       [-2.5, 4.3301300, -5, -5],
                       [-4.5, -0.8660250, -4, 1],
                       [-4, 0.0000000, -4, 0],
                       [-3.5, 0.8660250, -4, -1],
                       [-3, 1.7320500, -4, -2],
                       [-2.5, 2.5980800, -4, -3],
                       [-2, 3.4641000, -4, -4],
                       [-1.5, 4.3301300, -4, -5],
                       [-4.5, -2.5980800, -3, 3],
                       [-4, -1.7320500, -3, 2],
                       [-3.5, -0.8660250, -3, 1],
                       [-3, 0.0000000, -3, 0],
                       [-2.5, 0.8660250, -3, -1],
                       [-2, 1.7320500, -3, -2],
                       [-1.5, 2.5980800, -3, -3],
                       [-1, 3.4641000, -3, -4],
                       [-0.5, 4.3301300, -3, -5],
                       [0, 5.1961500, -3, -6],
                       [-3.5, -2.5980800, -2, 3],
                       [-3, -1.7320500, -2, 2],
                       [-2.5, -0.8660250, -2, 1],
                       [-2, 0.0000000, -2, 0],
                       [-1.5, 0.8660250, -2, -1],
                       [-1, 1.7320500, -2, -2],
                       [-0.5, 2.5980800, -2, -3],
                       [0, 3.4641000, -2, -4],
                       [0.5, 4.3301300, -2, -5],
                       [-3, -3.4641000, -1, 4],
                       [-2.5, -2.5980800, -1, 3],
                       [-2, -1.7320500, -1, 2],
                       [-1.5, -0.8660250, -1, 1],
                       [-1, 0.0000000, -1, 0],
                       [-0.5, 0.8660250, -1, -1],
                       [0, 1.7320500, -1, -2],
                       [0.5, 2.5980800, -1, -3],
                       [1, 3.4641000, -1, -4],
                       [1.5, 4.3301300, -1, -5],
                       [-2.5, -4.3301300, 0, 5],
                       [-2, -3.4641000, 0, 4],
                       [-1.5, -2.5980800, 0, 3],
                       [-1, -1.7320500, 0, 2],
                       [-0.5, -0.8660250, 0, 1],
                       [0, 0.0000000, 0, 0],
                       [0.5, 0.8660250, 0, -1],
                       [1, 1.7320500, 0, -2],
                       [1.5, 2.5980800, 0, -3],
                       [2, 3.4641000, 0, -4],
                       [2.5, 4.3301300, 0, -5],
                       [-1.5, -4.3301300, 1, 5],
                       [-1, -3.4641000, 1, 4],
                       [-0.5, -2.5980800, 1, 3],
                       [0, -1.7320500, 1, 2],
                       [0.5, -0.8660250, 1, 1],
                       [1, 0.0000000, 1, 0],
                       [1.5, 0.8660250, 1, -1],
                       [2, 1.7320500, 1, -2],
                       [2.5, 2.5980800, 1, -3],
                       [3, 3.4641000, 1, -4],
                       [-0.5, -4.3301300, 2, 5],
                       [0, -3.4641000, 2, 4],
                       [0.5, -2.5980800, 2, 3],
                       [1, -1.7320500, 2, 2],
                       [1.5, -0.8660250, 2, 1],
                       [2, 0.0000000, 2, 0],
                       [2.5, 0.8660250, 2, -1],
                       [3, 1.7320500, 2, -2],
                       [3.5, 2.5980800, 2, -3],
                       [0, -5.1961500, 3, 6],
                       [0.5, -4.3301300, 3, 5],
                       [1, -3.4641000, 3, 4],
                       [1.5, -2.5980800, 3, 3],
                       [2, -1.7320500, 3, 2],
                       [2.5, -0.8660250, 3, 1],
                       [3, 0.0000000, 3, 0],
                       [3.5, 0.8660250, 3, -1],
                       [4, 1.7320500, 3, -2],
                       [4.5, 2.5980800, 3, -3],
                       [1.5, -4.3301300, 4, 5],
                       [2, -3.4641000, 4, 4],
                       [2.5, -2.5980800, 4, 3],
                       [3, -1.7320500, 4, 2],
                       [3.5, -0.8660250, 4, 1],
                       [4, 0.0000000, 4, 0],
                       [4.5, 0.8660250, 4, -1],
                       [2.5, -4.3301300, 5, 5],
                       [3, -3.4641000, 5, 4],
                       [3.5, -2.5980800, 5, 3],
                       [4, -1.7320500, 5, 2],
                       [4.5, -0.8660250, 5, 1],
                       [5, 0.0000000, 5, 0],
                       [4.5, -2.5980800, 6, 3]],
                      columns=['mapx', 'mapy', 'mapxhoney', 'mapyhoney'])


def get_sim_freq(path_movescsv, chatty = False):
    """
    Read in simulation data from moves csv file, calculate for each field
    the relative frequency it was visited with and find corresponding color intensities
    representing those frequencies.
    Return the colors for each field and a dataframe with coordinates needed for plotting.
    """
    #### Dictionary für verschiedene Mappings #### -> map_dict.get(tuplPlot) gibt tuplhoney
    listPlot = []
    listhoney = []
    for i in range(mapmap.shape[0]):
        tuplPlot = (mapmap.iloc[i, 0], mapmap.iloc[i, 1])
        listPlot.append(tuplPlot)
        tuplhoney = (int(mapmap.iloc[i, 2]), int(mapmap.iloc[i, 3]))
        listhoney.append(tuplhoney)
    map_dict = dict(zip(listPlot, listhoney))
    if chatty: print(map_dict)

    #### Daten einlesen und besuchte Felder extrahieren ####
    df_move = pd.read_csv(path_movescsv, index_col=0)
    df_move['s1'] = df_move['s1'] - 6
    df_move['s2'] = df_move['s2'] - 6
    df_move['sim'] = df_move['sim'] + 1
    moveslist = []
    for i in range(df_move.shape[0]):
        temp = (df_move.loc[i, 's1'], df_move.loc[i, 's2'])
        moveslist.append(temp)

    #### Häufigkeiten der Felder ####
    counter = collections.Counter(moveslist)
    max_freq = counter.most_common(1)[0][1] + 1
    freq_dict = {}
    freq = []
    for coords in listPlot:  # Felder durchiterieren, dict bauen und liste in der richtigen Reihenfolge für Farben
        honeycoords = map_dict.get(coords)
        # print(coords, " - " , honeycoords)
        if coords == (0.0, 0.0):
            freq_dict[coords] = 0.5
            freq.append(0.5)
        elif counter.get(honeycoords) == None:  # keine Häufikeiten in counter heißt nicht besucht
            freq_dict[coords] = 0
            freq.append(0)
        else:  # Häufigkeit/max(Häufigkeiten) und abspeichern
            freq_dict[coords] = (counter.get(honeycoords) / max_freq)
            freq.append(counter.get(honeycoords) / max_freq)

    #### Farben festsetzen ####
    rgba_colors = np.zeros((len(freq), 4))
    rgba_colors[:, 0] = 1.0; rgba_colors[:, 1] = 0.7; rgba_colors[:, 2] = 0.2; rgba_colors[:, 3] = freq

    return rgba_colors, mapmap

def get_simdata_for_comparisons(evalcsv_path = "/content/drive/MyDrive/HC/Data/sim_train_eval.csv",
                                movecsv_path = "/content/drive/MyDrive/HC/Data/sim_train_move.csv",
                                chatty = False):
    """
    Read in simulation data.
    Then prepares the data and returns a dataframe with the following information:
    For each run, run_id and id of agents, which players were informed, which reward they
    received, how many moves the used, whether they arrived on the special payoff field and
    a dummy variable set to 0 indicating that empirical data is represented.
    Returns one additional list containing the group sizes arriving at a reward field for
    each run.
    """
    # read simulation data
    eval_df = pd.read_csv(evalcsv_path, index_col=0)
    move_df = pd.read_csv(movecsv_path, index_col=0)

    # get data from eval
    eval_df.rename(columns={'sim': 'run_id'}, inplace=True)  # consistent with empirical data
    eval_df.drop(['arrivals', 'remaining moves'], axis=1, inplace=True)
    eval_df = eval_df.astype({'reward': float})
    eval_df['sim'] = [1] * eval_df.shape[0]  # dummy for simulation -> 1
    if chatty: print(eval_df.head())

    # get data from move
    enddf = []
    # get end positions
    for run in move_df['sim'].unique():
        for pl in range(10):  # get end coordinates
            temp = move_df[(move_df['sim'] == run) & (move_df['agent'] == pl)]
            endcoords = (temp.iloc[-1, 4], temp.iloc[-1, 5])
            enddf.append([run, pl, temp.iloc[-1, 4], temp.iloc[-1, 5], endcoords])

    enddf = pd.DataFrame(enddf, columns=['run_id', 'id', 's1', 's2', 'tuple'])

    arrival_in_groups = list()
    for run in enddf['run_id'].unique():
        temp = enddf[enddf['run_id'] == run]
        # get frequencies of endfields
        fieldcounts = collections.Counter(temp['tuple'])
        # only keep reward fields
        rewfieldcounts = {k: v for k, v in fieldcounts.items() if
                          k in [(0, 3), (3, 0), (3, 9), (9, 3), (9, 12), (12, 9)]}
        arrival_in_groups.append([run, rewfieldcounts])

    return eval_df, arrival_in_groups

