import numpy as np
import pandas as pd
import collections

# List with lists for all fields on HoneyComb gamefield, each with 7 tuples: one needed for plotting
# and 6 needed for the gamefield's rotation to a desired format
map_list_all_turns = [[(4.5, 2.59808), (3, -3), (-3, -6), (-6, -3), (-3, 3), (3, 6), (6, 3)],
                      [(1.5, -4.3301300000000005), (4, 5), (5, 1), (1, -4), (-4, -5), (-5, -1), (-1, 4)],
                      [(2.0, -3.4641), (4, 4), (4, 0), (0, -4), (-4, -4), (-4, 0), (0, 4)],
                      [(2.5, -2.59808), (4, 3), (3, -1), (-1, -4), (-4, -3), (-3, 1), (1, 4)],
                      [(3.0, -1.73205), (4, 2), (2, -2), (-2, -4), (-4, -2), (-2, 2), (2, 4)],
                      [(3.5, -0.866025), (4, 1), (1, -3), (-3, -4), (-4, -1), (-1, 3), (3, 4)],
                      [(4.0, 0.0), (4, 0), (0, -4), (-4, -4), (-4, 0), (0, 4), (4, 4)],
                      [(4.5, 0.866025), (4, -1), (-1, -5), (-5, -4), (-4, 1), (1, 5), (5, 4)],
                      [(2.5, -4.3301300000000005), (5, 5), (5, 0), (0, -5), (-5, -5), (-5, 0), (0, 5)],
                      [(3.0, -3.4641), (5, 4), (4, -1), (-1, -5), (-5, -4), (-4, 1), (1, 5)],
                      [(3.5, -2.59808), (5, 3), (3, -2), (-2, -5), (-5, -3), (-3, 2), (2, 5)],
                      [(4.0, -1.73205), (5, 2), (2, -3), (-3, -5), (-5, -2), (-2, 3), (3, 5)],
                      [(4.5, -0.866025), (5, 1), (1, -4), (-4, -5), (-5, -1), (-1, 4), (4, 5)],
                      [(5.0, 0.0), (5, 0), (0, -5), (-5, -5), (-5, 0), (0, 5), (5, 5)],
                      [(4.5, -2.59808), (6, 3), (3, -3), (-3, -6), (-6, -3), (-3, 3), (3, 6)],
                      [(-4.5, 2.59808), (-6, -3), (-3, 3), (3, 6), (6, 3), (3, -3), (-3, -6)],
                      [(-5.0, 0.0), (-5, 0), (0, 5), (0, 5), (5, 0), (0, -5), (-5, -5)],
                      [(-4.5, 0.866025), (-5, -1), (-1, 4), (1, 5), (5, 1), (1, -4), (-4, -5)],
                      [(-4.0, 1.732), (-5, -2), (-2, 3), (2, 5), (5, 2), (2, -3), (-3, -5)],
                      [(-3.5, 2.59808), (-5, -3), (-3, 2), (3, 5), (5, 3), (3, -2), (-2, -5)],
                      [(-3.0, 3.4641), (-5, -4), (-4, 1), (4, 5), (5, 4), (4, -1), (-1, -5)],
                      [(-2.5, 4.3301300000000005), (-5, -5), (-5, 0), (5, 5), (5, 5), (5, 0), (0, -5)],
                      [(-4.5, -0.866025), (-4, 1), (1, 5), (5, 4), (4, -1), (-1, -5), (-5, -4)],
                      [(-4.0, 0.0), (-4, 0), (0, 4), (4, 4), (4, 0), (0, -4), (-4, -4)],
                      [(-3.5, 0.866025), (-4, -1), (-1, 3), (3, 4), (4, 1), (1, -3), (-3, -4)],
                      [(-3.0, 1.73205), (-4, -2), (-2, 2), (2, 4), (4, 2), (2, -2), (-2, -4)],
                      [(-2.5, 2.59808), (-4, -3), (-3, 1), (1, 4), (4, 3), (3, -1), (-1, -4)],
                      [(-2.0, 3.4641), (-4, -4), (-4, 0), (0, 4), (4, 4), (4, 0), (0, -4)],
                      [(-1.5, 4.3301300000000005), (-4, -5), (-5, -1), (-1, 4), (4, 5), (5, 1), (1, -4)],
                      [(-4.5, -2.59808), (-3, 3), (3, 6), (6, 3), (3, -3), (-3, -6), (-6, -3)],
                      [(-4.0, -1.73205), (-3, 2), (2, 5), (5, 3), (3, -2), (-2, -5), (-5, -3)],
                      [(-3.5, -0.866025), (-3, 1), (1, 4), (4, 3), (3, -1), (-1, -4), (-4, -3)],
                      [(-3.0, 0.0), (-3, 0), (0, 3), (3, 3), (3, 0), (0, -3), (-3, -3)],
                      [(-2.5, 0.866025), (-3, -1), (-1, 2), (2, 3), (3, 1), (1, -2), (-2, -3)],
                      [(-2.0, 1.73205), (-3, -2), (-2, 1), (1, 3), (3, 2), (2, -1), (-1, -3)],
                      [(-1.5, 2.59808), (-3, -3), (-3, 0), (0, 3), (3, 3), (3, 0), (0, -3)],
                      [(-1.0, 3.4641), (-3, -4), (-4, -1), (-1, 3), (3, 4), (4, 1), (1, -3)],
                      [(-0.5, 4.3301300000000005), (-3, -5), (-5, -2), (-2, 3), (3, 5), (5, 2), (2, -3)],
                      [(0.0, 5.19615), (-3, -6), (-6, -3), (-3, 3), (3, 6), (6, 3), (3, -3)],
                      [(-3.5, -2.59808), (-2, 3), (3, 5), (5, 2), (2, -3), (-3, -5), (-5, -2)],
                      [(-3.0, -1.73205), (-2, 2), (2, 4), (4, 2), (2, -2), (-2, -4), (-4, -2)],
                      [(-2.5, -0.866025), (-2, 1), (1, 3), (3, 2), (2, -1), (-1, -3), (-3, -2)],
                      [(-2.0, 0.0), (-2, 0), (0, 2), (2, 2), (2, 0), (0, -2), (-2, -2)],
                      [(-1.5, 0.866025), (-2, -1), (-1, 1), (1, 2), (2, 1), (1, -1), (-1, -2)],
                      [(-1.0, 1.73205), (-2, -2), (-2, 0), (0, 2), (2, 2), (2, 0), (0, -2)],
                      [(-0.5, 2.59808), (-2, -3), (-3, -1), (-1, 2), (2, 3), (3, 1), (1, -2)],
                      [(0.0, 3.4641), (-2, -4), (-4, -2), (-2, 2), (2, 4), (4, 2), (2, -2)],
                      [(0.5, 4.3301300000000005), (-2, -5), (-5, -3), (-3, 2), (2, 5), (5, 3), (3, -2)],
                      [(-3.0, -3.4641), (-1, 4), (4, 5), (5, 1), (1, -4), (-4, -5), (-5, -1)],
                      [(-2.5, -2.59808), (-1, 3), (3, 4), (4, 1), (1, -3), (-3, -4), (-4, -1)],
                      [(-2.0, -1.73205), (-1, 2), (2, 3), (3, 1), (1, -2), (-2, -3), (-3, -1)],
                      [(-1.5, -0.866025), (-1, 1), (1, 2), (2, 1), (1, -1), (-1, -2), (-2, -1)],
                      [(-1.0, 0.0), (-1, 0), (0, 1), (1, 1), (1, 0), (0, -1), (-1, -1)],
                      [(-0.5, 0.866025), (-1, -1), (-1, 0), (0, 1), (1, 1), (1, 0), (0, -1)],
                      [(0.0, 1.73205), (-1, -2), (-2, -1), (-1, 1), (1, 2), (2, 1), (1, -1)],
                      [(0.5, 2.59808), (-1, -3), (-3, -2), (-2, 1), (1, 3), (3, 2), (2, -1)],
                      [(1.0, 3.4641), (-1, -4), (-4, -3), (-3, 1), (1, 4), (4, 3), (3, -1)],
                      [(1.5, 4.3301300000000005), (-1, -5), (-5, -4), (-4, 1), (1, 5), (5, 4), (4, -1)],
                      [(-2.5, -4.3301300000000005), (0, 5), (5, 5), (5, 0), (0, -5), (-5, -5), (-5, 0)],
                      [(-2.0, -3.4641), (0, 4), (4, 4), (4, 0), (0, -4), (-4, -4), (-4, 0)],
                      [(-1.5, -2.59808), (0, 3), (3, 3), (3, 0), (0, -3), (-3, -3), (-3, 0)],
                      [(-1.0, -1.73205), (0, 2), (2, 2), (2, 0), (0, -2), (-2, -2), (-2, 0)],
                      [(-0.5, -0.866025), (0, 1), (1, 1), (1, 0), (0, -1), (-1, -1), (-1, 0)],
                      [(0.0, 0.0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0)],
                      [(0.5, 0.866025), (0, -1), (-1, -1), (-1, 0), (0, 1), (1, 1), (1, 0)],
                      [(1.0, 1.73205), (0, -2), (-2, -2), (-2, 0), (0, 2), (2, 2), (2, 0)],
                      [(1.5, 2.59808), (0, -3), (-3, -3), (-3, 0), (0, 3), (3, 3), (3, 0)],
                      [(2.0, 3.4641), (0, -4), (-4, -4), (-4, 0), (0, 4), (4, 4), (4, 0)],
                      [(2.5, 4.3301300000000005), (0, -5), (-5, -5), (-5, 0), (0, 5), (5, 5), (5, 0)],
                      [(-1.5, -4.3301300000000005), (1, 5), (5, 4), (4, -1), (-1, -5), (-5, -4), (-4, 1)],
                      [(-1.0, -3.4641), (1, 4), (4, 3), (3, -1), (-1, -4), (-4, -3), (-3, 1)],
                      [(-0.5, -2.59808), (1, 3), (3, 2), (2, -1), (-1, -3), (-3, -2), (-2, 1)],
                      [(0.0, -1.73205), (1, 2), (2, 1), (1, -1), (-1, -2), (-2, -1), (-1, 1)],
                      [(0.5, -0.866025), (1, 1), (1, 0), (0, -1), (-1, -1), (-1, 0), (0, 1)],
                      [(1.0, 0.0), (1, 0), (0, -1), (-1, -1), (-1, 0), (0, 1), (1, 1)],
                      [(1.5, 0.866025), (1, -1), (-1, -2), (-2, -1), (-1, 1), (1, 2), (2, 1)],
                      [(2.0, 1.73205), (1, -2), (-2, -3), (-3, -1), (-1, 2), (2, 3), (3, 1)],
                      [(2.5, 2.59808), (1, -3), (-3, -4), (-4, -1), (-1, 3), (3, 4), (4, 1)],
                      [(3.0, 3.4641), (1, -4), (-4, -5), (-5, -1), (-1, 4), (4, 5), (5, 1)],
                      [(-0.5, -4.3301300000000005), (2, 5), (5, 3), (3, -2), (-2, -5), (-5, -3), (-3, 2)],
                      [(0.0, -3.4641), (2, 4), (4, 2), (2, -2), (-2, -4), (-4, -2), (-2, 2)],
                      [(0.5, -2.59808), (2, 3), (3, 1), (1, -2), (-2, -3), (-3, -1), (-1, 2)],
                      [(1.0, -1.73205), (2, 2), (2, 0), (0, -2), (-2, -2), (-2, 0), (0, 2)],
                      [(1.5, -0.866025), (2, 1), (1, -1), (-1, -2), (-2, -1), (-1, 1), (1, 2)],
                      [(2.0, 0.0), (2, 0), (0, -2), (-2, -2), (-2, 0), (0, 2), (2, 2)],
                      [(2.5, 0.866025), (2, -1), (-1, -3), (-3, -2), (-2, 1), (1, 3), (3, 2)],
                      [(3.0, 1.73205), (2, -2), (-2, -4), (-4, -2), (-2, 2), (2, 4), (4, 2)],
                      [(3.5, 2.59808), (2, -3), (-3, -5), (-5, -2), (-2, 3), (3, 5), (5, 2)],
                      [(0.0, -5.19615), (3, 6), (6, 3), (3, -3), (-3, -6), (-6, -3), (-3, 3)],
                      [(0.5, -4.3301300000000005), (3, 5), (5, 2), (2, -3), (-3, -5), (-5, -2), (-2, 3)],
                      [(1.0, -3.4641), (3, 4), (4, 1), (1, -3), (-3, -4), (-4, -1), (-1, 3)],
                      [(1.5, -2.59808), (3, 3), (3, 0), (0, -3), (-3, -3), (-3, 0), (0, 3)],
                      [(2.0, -1.73205), (3, 2), (2, -1), (-1, -3), (-3, -2), (-2, 1), (1, 3)],
                      [(2.5, -0.866025), (3, 1), (1, -2), (-2, -3), (-3, -1), (-1, 2), (2, 3)],
                      [(3.0, 0.0), (3, 0), (0, -3), (-3, -3), (-3, 0), (0, 3), (3, 3)],
                      [(3.5, 0.866025), (3, -1), (-1, -4), (-4, -3), (-3, 1), (1, 4), (4, 3)],
                      [(4.0, 1.73205), (3, -2), (-2, -5), (-5, -3), (-3, 2), (2, 5), (5, 3)]]
# DataFrame obly with plotcoordinates and honeycomb coordinates
mapmap = pd.DataFrame([[-4.5, 2.5980800, -6, -3],
                       [-5, 0.0000000, -5, 0],
                       [-4.5, 0.8660250, -5, -1],
                       [-4, 1.7320000, -5, -2],
                       [-3.5, 2.5980800, -5, -3],
                       [-3, 3.4641000, -5, -4],
                       [-2.5, 4.3301300, -5, -5],
                       [-4.5, -0.8660250, -4, 1],
                       [-4, 0.0000000, -4, 0],
                       [-3.5, 0.8660250, -4, -1],
                       [-3, 1.7320500, -4, -2],
                       [-2.5, 2.5980800, -4, -3],
                       [-2, 3.4641000, -4, -4],
                       [-1.5, 4.3301300, -4, -5],
                       [-4.5, -2.5980800, -3, 3],
                       [-4, -1.7320500, -3, 2],
                       [-3.5, -0.8660250, -3, 1],
                       [-3, 0.0000000, -3, 0],
                       [-2.5, 0.8660250, -3, -1],
                       [-2, 1.7320500, -3, -2],
                       [-1.5, 2.5980800, -3, -3],
                       [-1, 3.4641000, -3, -4],
                       [-0.5, 4.3301300, -3, -5],
                       [0, 5.1961500, -3, -6],
                       [-3.5, -2.5980800, -2, 3],
                       [-3, -1.7320500, -2, 2],
                       [-2.5, -0.8660250, -2, 1],
                       [-2, 0.0000000, -2, 0],
                       [-1.5, 0.8660250, -2, -1],
                       [-1, 1.7320500, -2, -2],
                       [-0.5, 2.5980800, -2, -3],
                       [0, 3.4641000, -2, -4],
                       [0.5, 4.3301300, -2, -5],
                       [-3, -3.4641000, -1, 4],
                       [-2.5, -2.5980800, -1, 3],
                       [-2, -1.7320500, -1, 2],
                       [-1.5, -0.8660250, -1, 1],
                       [-1, 0.0000000, -1, 0],
                       [-0.5, 0.8660250, -1, -1],
                       [0, 1.7320500, -1, -2],
                       [0.5, 2.5980800, -1, -3],
                       [1, 3.4641000, -1, -4],
                       [1.5, 4.3301300, -1, -5],
                       [-2.5, -4.3301300, 0, 5],
                       [-2, -3.4641000, 0, 4],
                       [-1.5, -2.5980800, 0, 3],
                       [-1, -1.7320500, 0, 2],
                       [-0.5, -0.8660250, 0, 1],
                       [0, 0.0000000, 0, 0],
                       [0.5, 0.8660250, 0, -1],
                       [1, 1.7320500, 0, -2],
                       [1.5, 2.5980800, 0, -3],
                       [2, 3.4641000, 0, -4],
                       [2.5, 4.3301300, 0, -5],
                       [-1.5, -4.3301300, 1, 5],
                       [-1, -3.4641000, 1, 4],
                       [-0.5, -2.5980800, 1, 3],
                       [0, -1.7320500, 1, 2],
                       [0.5, -0.8660250, 1, 1],
                       [1, 0.0000000, 1, 0],
                       [1.5, 0.8660250, 1, -1],
                       [2, 1.7320500, 1, -2],
                       [2.5, 2.5980800, 1, -3],
                       [3, 3.4641000, 1, -4],
                       [-0.5, -4.3301300, 2, 5],
                       [0, -3.4641000, 2, 4],
                       [0.5, -2.5980800, 2, 3],
                       [1, -1.7320500, 2, 2],
                       [1.5, -0.8660250, 2, 1],
                       [2, 0.0000000, 2, 0],
                       [2.5, 0.8660250, 2, -1],
                       [3, 1.7320500, 2, -2],
                       [3.5, 2.5980800, 2, -3],
                       [0, -5.1961500, 3, 6],
                       [0.5, -4.3301300, 3, 5],
                       [1, -3.4641000, 3, 4],
                       [1.5, -2.5980800, 3, 3],
                       [2, -1.7320500, 3, 2],
                       [2.5, -0.8660250, 3, 1],
                       [3, 0.0000000, 3, 0],
                       [3.5, 0.8660250, 3, -1],
                       [4, 1.7320500, 3, -2],
                       [4.5, 2.5980800, 3, -3],
                       [1.5, -4.3301300, 4, 5],
                       [2, -3.4641000, 4, 4],
                       [2.5, -2.5980800, 4, 3],
                       [3, -1.7320500, 4, 2],
                       [3.5, -0.8660250, 4, 1],
                       [4, 0.0000000, 4, 0],
                       [4.5, 0.8660250, 4, -1],
                       [2.5, -4.3301300, 5, 5],
                       [3, -3.4641000, 5, 4],
                       [3.5, -2.5980800, 5, 3],
                       [4, -1.7320500, 5, 2],
                       [4.5, -0.8660250, 5, 1],
                       [5, 0.0000000, 5, 0],
                       [4.5, -2.5980800, 6, 3]],
                      columns=['mapx', 'mapy', 'mapxhoney', 'mapyhoney'])
# dict with the €€-field for run_ids in empirical data
doubles = {605: (3, 6), 705: (3, -3), 805: (3, -3), 905: (3, 6), 1005: (3, -3), 1105: (6, 3), 1205: (6, 3),
           1305: (-3, -6), 1405: (-6, -3), 1505: (-6, -3), 1605: (-3, -6), 1705: (-3, -6), 1805: (-3, 3),
           1905: (-6, -3), 2005: (3, 6), 2105: (-6, -3), 2205: (-6, -3), 2305: (3, -3), 2405: (-6, -3), 2505: (3, 6),
           2605: (-3, 3), 2705: (6, 3), 2805: (3, -3), 2905: (-3, 3), 3005: (-3, -6), 3105: (6, 3), 3205: (6, 3),
           3305: (3, 6), 3405: (-3, -6), 3505: (3, 6), 3605: (-3, 3), 3705: (-6, -3), 3805: (-3, 3), 3905: (-3, 3),
           4005: (-3, -6), 4105: (3, 6), 4205: (-3, -6), 4305: (3, 6), 4405: (3, 6), 4505: (-6, -3)}


def getPlotCoordsFor(EEField=(3, -3)):
    """
    Returns dictionary mapping fields to coordinates needed for plotting.
    """
    if EEField == (3, -3):
        turn = 1
    elif EEField == (-3, -6):
        turn = 2
    elif EEField == (-6, -3):
        turn = 3
    elif EEField == (-3, 3):
        turn = 4
    elif EEField == (3, 6):
        turn = 5
    elif EEField == (6, 3):
        turn = 6
    plotlist = []
    gamefieldlist = []
    for i in np.arange(97):
        plotlist.append(map_list_all_turns[i][0])
        gamefieldlist.append(map_list_all_turns[i][turn])

    map_dict = dict(zip(gamefieldlist, plotlist))
    return map_dict


def getNewHoneyCoordsFor(EEField=(3, -3)):
    """
    Returns dictionary that allows to rotate the data as if the special payoff field was (3,-3).
    """
    if EEField == (3, -3):
        turn = 1
    elif EEField == (-3, -6):
        turn = 2
    elif EEField == (-6, -3):
        turn = 3
    elif EEField == (-3, 3):
        turn = 4
    elif EEField == (3, 6):
        turn = 5
    elif EEField == (6, 3):
        turn = 6
    oldlist = []
    newlist = []
    for i in np.arange(97):
        newlist.append(map_list_all_turns[i][1])
        oldlist.append(map_list_all_turns[i][turn])

    map_dict = dict(zip(oldlist, newlist))
    return map_dict


def get_emp_freq(empdata_path="/content/drive/MyDrive/HC/Data/DatenAusJohannesMail", chatty=False):
    """
    Read in empirical data and rotate it as if the special reward field would have been the upper
    right reward field. For each field, calculate the relative frequency it was visited with
    in all runs and find corresponding color intensities representing those frequencies.
    Return the colors for each field and a dataframe with coordinates needed for plotting.
    """
    empdata = pd.read_csv(empdata_path, sep=';')
    empdata = empdata.drop(['Unnamed: 7', 'R_GM_TYPE'], axis=1)

    # RUN_ID = sim
    # PL_ID = agent
    # M_MS (millisecunden glaube ich) ~ turn
    # M_S1 = s1
    # M_S2 = s2
    # M_NOT_MOVED = ?
    # ? = action
    empdata = empdata.drop(empdata[empdata['M_NOT_MOVED'] == True].index,
                           axis=0)  # entferne die not-moved Einträge (mit Agent = -1 am Anfang jedes Spiels und wenn ein Spieler stehengeblieben ist am Ende)
    empdata = empdata.drop('M_NOT_MOVED', axis=1)

    empdata.columns = ["sim", "agent", "turn", "s1", "s2"]
    nrows = empdata.shape[0]
    empdata = empdata.sort_values(by=['sim', 'turn'], axis=0)
    if chatty: print(empdata.head())
    if chatty: print(empdata['sim'].unique())

    run_ids = empdata['sim'].unique()
    all_moves = pd.DataFrame()
    for run in run_ids:
        # get €€ field
        eefield = doubles.get(run)

        # transform if necessary
        if eefield != (3, -3):
            hondict = getNewHoneyCoordsFor(eefield)
            # get data of this run_id
            tempdata = empdata[empdata['sim'] == run]
            newdata = tempdata.drop(['s1', 's2'], axis=1)

            s1 = []
            s2 = []
            # iterate through data and replace coordinates
            for i in range(tempdata.shape[0]):
                # get old coords
                oldcoords = (tempdata.iloc[i, -2], tempdata.iloc[i, -1])
                newcoords = hondict.get(oldcoords)
                s1.append(newcoords[0])
                s2.append(newcoords[1])
            newdata['s1'] = s1
            newdata['s2'] = s2
            if chatty: print(newdata.head())
        else:  # €€ is already at (3, -3)
            newdata = tempdata.copy()
            if chatty: print(newdata.head())

        all_moves = all_moves.append(newdata)

    # list of honeycomb coordinates as tuples
    listhoney = [(int(mapmap.iloc[i, 2]), int(mapmap.iloc[i, 3])) for i in range(mapmap.shape[0])]
    # list of tuples in moves (visited fields)
    moveslist = [(all_moves.iloc[i, 3], all_moves.iloc[i, 4]) for i in range(all_moves.shape[0])]

    #### Häufigkeiten der Felder ####
    counter = collections.Counter(moveslist)
    if chatty: print(counter)
    max = counter.most_common(1)[0][1] + 1
    freq_dict = {}
    freq = []
    if chatty: print("max freq: ", max)
    for coords in listhoney:  # Felder durchiterieren, dict bauen und liste in der richtigen Reihenfolge für Farben
        if coords == (0, 0):
            freq_dict[coords] = 0.5
            freq.append(0.5)
        elif counter.get(coords) == None:  # keine Häufikeiten in counter heißt nicht besucht
            freq_dict[coords] = 0
            freq.append(0)
        else:  # Häufigkeit/max(Häufigkeiten) und abspeichern
            freq_dict[coords] = (counter.get(coords) / max)
            freq.append(counter.get(coords) / max)
    if chatty: print(freq_dict)

    rgba_colors = np.zeros((len(freq), 4))
    rgba_colors[:, 0] = 0.1; rgba_colors[:, 1] = 0.4; rgba_colors[:, 2] = 0.0; rgba_colors[:, 3] = freq  # grau

    return rgba_colors, mapmap


def get_empdata_for_comparisons(empdata_path = '/content/drive/MyDrive/HC/Data/DatenAusJohannesMail',
                                doubleinfo_path = '/content/drive/MyDrive/HC/Data/doubleeuropads.csv',
                                chatty = False):
    """
    Read in empirical data and information on where the special payoff field was and who the
    informed agents were for each run in the experiment.
    Then prepares the data, creates and returns a dataframe with the following information:
    For each run, run_id and id of players, which players were informed, which reward they
    received, how many moves the used, whether they arrived on the special payoff field and
    a dummy variable set to 0 indicating that empirical data is represented.
    Returns one additional list containing the group sizes arriving at a reward field for
    each run.
    """

    empdata = pd.read_csv(empdata_path, sep=';')
    doubleinfo = pd.read_csv(doubleinfo_path, header=0, sep=';')
    rewarddata = empdata.drop(empdata[empdata['PL_ID'] == -1].index, axis=0).copy()
    empdata = empdata.drop(empdata[empdata['M_NOT_MOVED'] == True].index, axis=0)  # entferne die not-moved Einträge (mit Agent = -1 am Anfang jedes Spiels und wenn ein Spieler stehengeblieben ist am Ende)
    empdata = empdata.drop(['Unnamed: 7', 'R_GM_TYPE', 'M_NOT_MOVED'], axis=1)
    if chatty: print(empdata.head())

    run_ids = np.repeat(empdata['RUN_ID'].unique(), 10)
    pl_ids = empdata['PL_ID'].unique()

    # for iterating through runs and player
    newemp = pd.DataFrame()
    newemp['run_id'] = run_ids
    newemp['id'] = pl_ids
    if chatty: print(newemp.head())

    # rewards & arrived
    enddf = []
    # get end positions
    for i in range(newemp.shape[0]):  # 400 run und player Kombinationen
        run, pl = newemp.iloc[i,]
        temp = rewarddata[(rewarddata['RUN_ID'] == run) & (rewarddata['PL_ID'] == pl)]
        endcoords = (temp.iloc[-1, 4], temp.iloc[-1, 5])
        if doubleinfo[(doubleinfo['RUN_ID'] == run) & (doubleinfo['PL_ID'] == pl)].empty:
            informed = 0
        else:
            informed = 1
        enddf.append([run, pl, temp.iloc[-1, 4], temp.iloc[-1, 5], endcoords, informed])

    enddf = pd.DataFrame(enddf, columns=['run_id', 'id', 's1', 's2', 'tuple', 'informed'])
    if chatty: print(enddf[:10])

    arrival_in_groups = []
    rewardsdf = pd.DataFrame(columns=['run_id', 'id', 's1', 's2', 'tuple', 'informed', 'reward', 'arrived'])
    # calculate rewards
    for run in empdata['RUN_ID'].unique():
        if chatty: print("################ RUN " + str(run) + ' #####################')
        # get end positions of run
        temp = enddf[enddf['run_id'] == run]
        # get max payoff
        eefield = doubles.get(run)
        # get frequencies of endfields
        fieldcounts = collections.Counter(temp['tuple'])
        # only keep reward fields
        rewfieldcounts = {k: v for k, v in fieldcounts.items() if
                          k in [(3, -3), (6, 3), (3, 6), (-3, 3), (-6, -3), (-3, -6)]}
        arrival_in_groups.append([run, rewfieldcounts])
        # get payoffs per player and arrived
        rewards = []
        arriveds = []
        for pl in range(10):
            endcoord = temp.iloc[pl, 4]
            # if temp.iloc[pl, 5] == 1:print('Endet auf:' + str(endcoord) + ", €€ auf " + str(eefield))
            if rewfieldcounts.get(endcoord) == None:
                reward = 0
            elif temp.iloc[pl, 5] == 1:  # informed
                if endcoord == eefield:  # auf €€
                    reward = float(rewfieldcounts.get(endcoord)) * 1
                    arrived = 1
                else:  # auf €
                    reward = float(rewfieldcounts.get(endcoord)) * 0.5
                    arrived = 0
            elif temp.iloc[pl, 5] == 0:  # uninformed, egal wo
                reward = float(rewfieldcounts.get(endcoord)) * 0.5
                if endcoord == eefield:  # auf €€
                    arrived = 1
                else:  # auf €
                    arrived = 0
            else:
                print("ISSUE IN run =" + run + " player " + pl)
            rewards.append(reward)
            arriveds.append(arrived)
        temp['reward'] = rewards
        temp['arrived'] = arriveds
        if chatty: print(temp.head())

        rewardsdf = rewardsdf.append(temp)

    moves = []
    for i in range(newemp.shape[0]):
        run = newemp.iloc[i, 0]
        pl = newemp.iloc[i, 1]
        temp = empdata[(empdata['RUN_ID'] == run) & (empdata['PL_ID'] == pl)]
        moves.append(temp.shape[0])
    if chatty: print(moves)
    rewardsdf['moves'] = moves
    rewardsdf['sim'] = np.zeros(400, dtype=int)  # Dummy for simulation -> 0

    data = rewardsdf.drop(['s1', 's2', 'tuple'], axis=1)
    data = data[['run_id', 'id', 'informed', 'reward', 'moves', 'arrived', 'sim']]

    return data, arrival_in_groups
