import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

rgba_colors = np.array([[0.1, 0.4, 0.0, 1.0], [1.0, 0.7, 0.2, 1]]) # custom colors for experimental and simulation data.

def make_freq_plot(rgba_colors, mapmap, plotname, minority = True,
                   output_path= '/content/drive/MyDrive/HC/Data/PräsiGraphenFinal'):
    """
    Plots the HoneyComb gamefield colored by frequency of visits, optionally with or whithout
    €€ on the special payoff field.
    """
    sns.set_style({'font.family': 'serif', 'font.serif': 'Times New Roman'})
    size_outer = 840
    size_inner = 640

    fig, ax = plt.subplots(figsize=(5, 5.26))
    # Felderumrandungen
    plt.scatter(mapmap['mapx'], mapmap['mapy'], marker='h', s=size_outer, color='k')
    plt.scatter(mapmap['mapx'], mapmap['mapy'], marker='h', s=size_inner, color='w')
    # Einfärbung nach frequencies
    plt.scatter(mapmap['mapx'], mapmap['mapy'], marker='h', s=size_inner + 50, color=rgba_colors)
    shift_left = 0.23
    shift_down = 0.23
    if minority:
        ax.text(4.5 - 0.41, 2.59808 - 0.23, "€€", fontsize=15)  # entspricht # (3, -3)
    else:
        ax.text(4.5 - 0.41, 2.59808 - 0.23, " €", fontsize=15)  # entspricht # (3, -3)
    ax.text(4.5 - shift_left, -2.59808 - shift_down, "€ ", fontsize=15)  # (6, 3)
    ax.text(-4.5 - shift_left, 2.59808 - shift_down, "€ ", fontsize=15)  # (-6, -3)
    ax.text(-4.5 - shift_left, -2.59808 - shift_down, "€ ", fontsize=15)  # (-3, 3)
    ax.text(0 - shift_left, 5.19615 - shift_down, "€ ", fontsize=15)  # (-3, -6)
    ax.text(0 - shift_left, -5.19615 - shift_down, "€ ", fontsize=15)  # (3, 6)

    plt.xlim([-6.5, 6.5])
    plt.ylim([-6.5, 6.5])
    ax.axes.get_xaxis().set_ticks([])
    ax.axes.get_yaxis().set_ticks([])
    ax.axes.get_xaxis().set_visible(False)
    ax.axes.get_yaxis().set_visible(False)
    np.vectorize(lambda ax: ax.axis('off'))(ax)

    plt.savefig(output_path + "/" + plotname + ".png", dpi=700, bbox_inches='tight')


def merge_sim_emp_data_for_comparisons(sim_data, emp_data):
    """
    Merges simulation and empirical data for comparative plots.
    """
    df = emp_data.append(sim_data)
    df.reset_index(drop=True, inplace=True)
    df = pd.DataFrame(df)
    return df

def plot_freq_of_rewards(df, plotname = 'relfrequencyofrewards',
                   output_path= '/content/drive/MyDrive/HC/Data/PräsiGraphenFinal'):
    """
    For each reward, the relative frequency it was paid out to players/agents is plotted.
    """

    sns.set_style({'font.family': 'serif', 'font.serif': 'Times New Roman'})

    fig, freq_plot = plt.subplots(1, 1, figsize = [8,6])
    freq_plot = sns.histplot(data=df, x='reward', hue='sim', multiple='dodge', stat='probability', palette=rgba_colors,
                             discrete=True, common_norm=False, alpha = 1)
    freq_plot.set_xticks(range(11))
    plt.legend(loc='upper right', labels=['Simulation', 'Experiment'])
    plt.xlabel('Reward')
    plt.ylabel('Relative Frequency')
    #plt.title('Relative Frequency of Rewards', fontsize=14)

    plt.savefig(output_path + "/" + plotname +".png", dpi=700, bbox_inches='tight')

def plot_amount_of_moves(df, plotname = 'relfrequencyofmoves',
                   output_path= '/content/drive/MyDrive/HC/Data/PräsiGraphenFinal'):
    sns.set_style({'font.family': 'serif', 'font.serif': 'Times New Roman'})

    fig1, remain_plot = plt.subplots(1, 1, figsize = [8,6])
    remain_plot = sns.histplot(data=df, x='moves', hue='sim', multiple='dodge', stat='probability', palette=rgba_colors,
                               discrete=True, common_norm=False, alpha = 1)
    remain_plot.set_xticks(range(16))
    plt.legend(loc='upper right', labels=['Simulation', 'Experiment'])
    plt.xlabel("Moves")
    plt.ylabel("Relative Frequency")
    #plt.title('Amount of Moves made during Game', fontsize=14)

    plt.savefig(output_path + "/" + plotname + ".png", dpi=700, bbox_inches='tight')

def plot_arrived_special_payoff(df, plotname = 'relfrequencyofarriveds',
                   output_path= '/content/drive/MyDrive/HC/Data/PräsiGraphenFinal'):
    """
    The share of uninformed agents that landed on the special reward field out of all uninformed
    agents is plotted and analogously for the informed agents.
    """
    # get frequencies of arrived with regard to informed
    temp_emp = pd.DataFrame(df[df['sim']==0].pivot_table(columns = ['arrived'], index = ['informed'], values = 'id', aggfunc = 'count'))
    temp_sim = pd.DataFrame(df[df['sim']==1].pivot_table(columns = ['arrived'], index = ['informed'], values = 'id', aggfunc = 'count'))
    temp = temp_emp.append(temp_sim)
    temp = temp.reset_index()
    temp.columns = ['informed', 'not_arrived', 'arrived']
    temp['sim'] = [0,0,1,1]
    emp_size = df[df['sim']==0].shape[0]
    sim_size = df[df['sim']==1].shape[0]
    temp['n'] = [emp_size*0.8, emp_size*0.2, sim_size*0.8, sim_size*0.2]
    temp['freq'] = [temp.loc[i,'arrived']/temp.loc[i,'n'] for i in range(temp.shape[0])]


    fig2, field = plt.subplots(1, 1, figsize = [8,6])
    field = sns.barplot(data=temp, y='freq', x='informed', palette=rgba_colors, hue='sim', saturation = 1.0
                        )
    field.set_xticklabels(['Uninformed Majority', 'Informed Minority'])

    handles, labels = plt.gca().get_legend_handles_labels()
    plt.legend([handles[1], handles[0]], ['Simulation', 'Experiment'])

    plt.ylabel("Relative Frequency")
    plt.xlabel('Agents on the €€-Field')

    plt.savefig(output_path + "/" + plotname + ".png", dpi=700, bbox_inches='tight')

def plot_rel_freq_type_of_field(df, plotname = 'relfrequencytypeoffield',
                   output_path= '/content/drive/MyDrive/HC/Data/PräsiGraphenFinal'):
    """
    For each type of field (normal, payoff, special payoff) the relative frequency of
    players/agents ending on it is plotted.
    """
    sns.set_style({'font.family': 'serif', 'font.serif': 'Times New Roman'})

    arrived = df['arrived'][df['sim'] == 0]
    reward = df['reward'][df['sim'] == 0]
    print(arrived[1])
    sum_arrived = 0
    sum_failed = 0
    sum_lost = 0
    for i in range(df[df['sim'] == 0].shape[0]):
        if arrived[i] == 1:
            sum_arrived += 1
        elif reward[i] > 0:
            sum_failed += 1
        else:
            sum_lost += 1

    norm = sum_arrived + sum_failed + sum_lost
    dexp_win = sum_arrived / norm
    dexp_fail = (sum_failed / norm) / 5
    dexp_lost = sum_lost / norm

    # simulation
    sim_arrived = df['arrived'][df['sim'] == 1].reset_index(drop=True)
    sim_reward = df['reward'][df['sim'] == 1].reset_index(drop=True)

    sim_sum_arrived = 0
    sim_sum_failed = 0
    sim_sum_lost = 0
    for i in range(df[df['sim'] == 1].shape[0]):
        if sim_arrived[i] == 1:
            sim_sum_arrived += 1
        elif sim_reward[i] > 0:
            sim_sum_failed += 1
        else:
            sim_sum_lost += 1

    norm = sim_sum_arrived + sim_sum_failed + sim_sum_lost
    dsim_win = sim_sum_arrived / norm
    dsim_fail = (sim_sum_failed / norm) / 5
    dsim_lost = sim_sum_lost / norm

    fig, ax2 = plt.subplots(1, 1, figsize = [8,6])
    fig.subplots_adjust(left=.13, bottom=.16, right=.87, top=.97)

    ax2.set_xlabel('Type of Reached Field')
    ax2.set_ylabel('Relative Frequency')

    bar_sim = plt.bar(('0', '€', '€ / €€'), (dsim_lost, dsim_fail, dsim_win), align='edge', width=0.32,
                      color=rgba_colors[1])
    bar_exp = plt.bar(('0', '€', '€ / €€'), (dexp_lost, dexp_fail, dexp_win), align='edge', width=-0.32,
                      color=rgba_colors[0])

    plt.legend((bar_sim, bar_exp), ('Simulation', 'Experiment'))
    plt.subplots_adjust(wspace=.0)
    plt.gcf().subplots_adjust(bottom=0.21)

    plt.savefig(output_path + "/" + plotname + ".png", dpi=700, bbox_inches='tight')

def plot_freq_of_grouparrivalsizes(sim_df_group_arrival, emp_df_group_arrival,
                                   plotname='relfrequencyofgrouparrivalsizes',
                                   output_path='/content/drive/MyDrive/HC/Data/PräsiGraphenFinal'):
    """
    For each run, the biggest groupsize of players/agents arriving on a reward field is computed.
    Then, for each groupsize, the relative frequency of its occurences in the data is plotted.
    """
    sns.set_style({'font.family': 'serif', 'font.serif': 'Times New Roman'})

    empgroups = pd.DataFrame(
        [[runinfo[0], np.max(np.array(pd.Series(runinfo[1].values(), index=runinfo[1].keys()))), 0] for runinfo in
         emp_df_group_arrival], columns=['run', 'groupsize', 'sim'])
    simgroups = pd.DataFrame(
        [[runinfo[0], np.max(np.array(pd.Series(runinfo[1].values(), index=runinfo[1].keys()))), 1] for runinfo in
         sim_df_group_arrival], columns=['run', 'groupsize', 'sim'])
    df = simgroups.append(empgroups)
    df.reset_index(drop=True, inplace=True)
    df = pd.DataFrame(df)

    fig, freq_plot = plt.subplots(1, 1, figsize = [8,6])
    freq_plot = sns.histplot(data=df, x='groupsize', hue='sim', multiple='dodge', stat='probability',
                             palette=rgba_colors,
                             discrete=True, common_norm=False, alpha = 1)
    freq_plot.set_xticks(range(11))
    plt.legend(loc='upper left', labels=['Simulation', 'Experiment'])
    plt.xlabel('Largest Groupsize per Game')
    plt.ylabel('Relative Frequency')
    #plt.title('Biggest Amount of Agents/Players on Same Payoff Field per Game', fontsize=14)

    plt.savefig(output_path + "/" + plotname + ".png", dpi=700, bbox_inches='tight')
    plt.show()
