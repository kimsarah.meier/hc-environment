from gym.envs.registration import register

register(
    id='honeycomb-v0',
    entry_point='gym_honeycomb.envs:HoneyCombEnv',
)
register(
    id='honeycomb-minority-v0',
    entry_point='gym_honeycomb.envs:HoneyCombMinorityEnv',
)
register(
    id='honeycomb-sim-v0',
    entry_point='gym_honeycomb.envs:HoneyCombSimulationEnv',
)