import numpy as np
import gym
from gym.spaces import Box
from gym_honeycomb.game.gamelogic import HoneyCombGame
from pprint import PrettyPrinter
import copy
import pandas as pd


class HoneyCombEnv(gym.Env):
    """ The HoneyComb Game \n
    Observation: Box(33,)
        Agent Positions,
        Payoff Fields, \n
        Remaining moves
    Actions: Discrete(7)
        'UP', 'DOWN_RIGHT', 'DOWN_LEFT', 'DOWN', 'UP_LEFT', 'UP_RIGHT', 'PASS'
    Reward: The Number of Agents on the same reward field.
    Episode Termination:
        End of Experiment, all 15 moves used. \n
        Episode length is greater than max_turns. \n
        All agents arrive on one of the payoff fields.
    """

    def __init__(self, max_turns=600, verbose=False, verbose_state=False):
        self.verbose = verbose
        self.verbose_state = verbose_state

        # action space: UP, DOWN_RIGHT, DOWN_LEFT, DOWN, UP_LEFT, UP_RIGHT, PASS
        self.action_space = gym.spaces.Discrete(7)
        # observation space: what the agent can see in the environment
        self.observation_space = (Box(-np.inf, np.inf, shape=(33,)))

        self.max_turns = max_turns
        self.game = HoneyCombGame()
        self.done = None
        self.store_moves = []
        self.ind_reward = None
        self.lag_observations = None
        self.dataframe_check = None
        self.dataframe_move = None
        self.dataframe_eval = None

        # Call reset here to initialize gamefield
        self.reset()

    def step(self, action, verbose=None, verbose_state=None):
        """
        Implementation of the agent-environment interaction.
        Performs one time step of environment dynamics.
        :param action: an action provided by the agent
        :param verbose: bool to print pd.DataFrame with coordinates of agents from game
        :param verbose_state: bool to print current state info during game (default is False)
        :return: state (object), reward (float), done (boolean), info (dict)
        """

        if verbose is None:
            verbose = self.verbose
        if verbose_state is None:
            verbose_state = self.verbose_state

        reward_sum = 0
        old_position = copy.copy(self.game.agent_positions[self.game.current_agent])

        # Information dict, mostly for debug
        info = {
            'Turn': self.game.turns_count,
            'Move_type': None,
            'Agent_ID': self.game.current_agent,
            'Remaining moves': None,
            'Reward': 0,
            'Old position': old_position,
            'New position': None
        }

        # Check if action is in discrete action space (0-6)
        assert self.action_space.contains(action), "%r (%s) invalid " % (action, type(action))

        # action 6: 'PASS'
        if action == 6:
            self.game.storage[self.game.current_agent].append(self.game.agent_positions[self.game.current_agent])

            # Update info: Agent stays at current position
            info['Move_type'] = 'PASS'
            info['New position'] = old_position
            valid_move = True

        else:
            # Get coordinates from old position and change in coordinates with action (delta row/column)
            r0, c0 = old_position
            dr, dc = self.game.ACTIONS[action]
            new_position = (r0 + dr, c0 + dc)
            valid_move = self.game.check_action(new=new_position)

            # If valid_move=True update of game parameters
            if valid_move:
                self.game.agent_positions[self.game.current_agent] = new_position
                self.game.update_gamefield()
                self.game.remaining_moves[self.game.current_agent] -= 1

                s1, s2 = new_position
                self.store_moves.append([self.game.turns_count,
                                         self.game.current_agent,
                                         action,
                                         s1, s2])

                # Update info:
                info['Move_type'] = self.game.ACTIONS_NAME[action]
                info['New position'] = self.game.agent_positions[self.game.current_agent]

            else:
                # Update info: Agent stays at current position
                info['Move_type'] = 'not moved'
                info['New position'] = old_position

            self.game.storage[self.game.current_agent].append(self.game.agent_positions[self.game.current_agent])

        # Update info:
        info['Remaining moves'] = self.game.remaining_moves[self.game.current_agent]

        # Update game parameter
        self.game.turns_count += 1
        self.make_next_agent(valid_move=valid_move)

        # Check arrived: all agents on payoff field
        all_arrived = self.check_positions

        # Environment is done when max_turns is reached or no agent is able to move
        if (self.game.turns_count > self.max_turns) or self.done or all_arrived:
            self.done = True
            self.ind_reward, reward_sum = self.reward_payoff_field
            info['Reward'] = reward_sum

            self.store()

            if verbose:
                print('Agent coordinates through the game: \n', self.make_check_df)
                print('Payoff fields: ', self.game.PAYOFF_FIELD)
                print('Rewards:', self.ind_reward)

        # Print option for debug
        if verbose_state:
            PrettyPrinter(indent=4).pprint(info)

        # Update lag observation
        num_movable_agents = len(self.game.movable_agent())
        if num_movable_agents > 1:
            if self.game.turns_count % num_movable_agents == 0:
                self.lag_observations = copy.copy(self.game.agent_positions)
        else:
            self.lag_observations = copy.copy(self.game.agent_positions)

        return self.get_observations, reward_sum, self.done, info

    # At the end of an episode call reset_game
    def reset(self):
        """
        At the end of an episode call reset_game to initialize a new gamefield.
        :return: Numpy array of current state
        """
        # Initialize a new gamefield
        self.game.reset_game()

        # Initialize of lag obs
        self.lag_observations = copy.copy(self.game.agent_positions)

        # Delete old stored info
        self.dataframe_check = None
        self.dataframe_move = None
        self.dataframe_eval = None

        return self.get_observations

    # reward function
    @property
    def reward_payoff_field(self):
        """
        The six payoff fields yield a reward of 1, if the agent ends the game on the special field.
        The payoff is multiplied by the number of agents on the same reward field.
        :return: A list of individual agent reward and the summarized rewards.
        """
        reward_list = []
        for agent in range(self.game.agents):
            n = 0
            if self.game.agent_positions[agent] in self.game.PAYOFF_FIELD:
                for pos in range(self.game.agents):
                    if self.game.agent_positions[agent] == self.game.agent_positions[pos]:
                        n += 1
            reward_list.append(n)
        return reward_list, sum(reward_list)

    @property
    def check_positions(self):
        """
        Checks if all agents arrived on any of the payoff fields.
        :return: A bool.
        """
        all_arrived = None
        for agent in range(self.game.agents):
            if self.game.agent_positions[agent] in self.game.PAYOFF_FIELD:
                all_arrived = True
            else:
                all_arrived = False
                break

        return all_arrived

    @property
    def get_observations(self):
        """
        Reflects the current state of the environment. Observation includes current agent position, other agent
        positions and reward fields as coordinates as well as the remaining moves of the current agent.
        :return: A list.
        """
        observation = None
        coord = copy.copy(self.lag_observations)

        if self.game.current_agent in range(self.game.agents):
            obs = [coord.pop(self.game.current_agent)]
            obs += coord
            obs += self.game.PAYOFF_FIELD
            obs_array = np.array(obs)
            observation = np.append(obs_array, self.game.remaining_moves[self.game.current_agent])

        if self.done:
            # If the environment is done: return initial obs
            observation = [6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
                           0, 3, 3, 0, 3, 9, 9, 3, 9, 12, 12, 9, 15]

        # Make sure observation has length of observation space
        assert len(observation) == 33
        return observation

    def make_next_agent(self, valid_move):
        """
        Updates next agent. If no agent has any moves left the environment is done.
        :param valid_move: bool to determine if the move made by the agent is a valid action
        :return: -
        """
        if valid_move:
            self.game.current_agent, self.done = self.game.next_agent()
        else:
            # Do not change agent if current agent did not make a valid move
            self.done = False

    @property
    def positions(self):
        """
        Returns current agent positions as dict.
        :return: dict
        """
        pos = dict()
        for a in range(self.game.agents):
            pos[a] = self.game.agent_positions[a]
        return pos

    @property
    def agent(self):
        """
        Returns the current agent.
        :return: A number.
        """
        return self.game.current_agent

    @property
    def make_check_df(self):
        """
        Stores the coordinates of the finished game for further usage.
        :return: pd.Dataframe
        """
        df = pd.DataFrame(self.game.storage)
        df.fillna(value='no_move', inplace=True)
        if self.done:
            df['end_position'] = self.game.agent_positions
        else:
            print('The environment is not done yet!')
        return df

    @property
    def make_df_move(self):
        """
        Returns a dataframe of agent, action and move.
        :return: A dataframe.
        """
        return pd.DataFrame(self.store_moves, columns=['turn', 'agent', 'action', 's1', 's2'])

    @property
    def make_df_eval(self):
        """
        Returns a dataframe for the evaluation of performance.
        :return: A dataframe.
        """
        df = None
        if self.done:
            on_payoff_field = []
            for i in range(len(self.ind_reward)):
                if self.game.agent_positions[i] == self.game.PAYOFF_SPECIAL[0]:
                    on_payoff_field .append(1)
                else:
                    on_payoff_field .append(0)

            df = pd.DataFrame({
                'id': [*range(self.game.agents)],
                'informed': [0] * 8 + [1] * 2,
                'reward': self.ind_reward,
                'remaining moves': self.game.remaining_moves,
                'arrived': on_payoff_field,
                'arrivals': [sum(on_payoff_field)] * self.game.agents
            })
        else:
            print('The environment is not done yet!')
        return df

    @property
    def df_check(self):
        """
        Attribute to store the dataframe check containing coordinates throughout the game of every agent.
        :return: A dataframe.
        """
        return self.dataframe_check

    @property
    def df_move(self):
        """
        Attribute to store the dataframe move containing information about coordinates, action, available moves for
        every agent.
        :return: A dataframe.
        """
        return self.dataframe_move

    @property
    def df_eval(self):
        """
        Attribute to store the dataframe containing information for evaluation of the game.
        :return: A dataframe.
        """
        return self.dataframe_eval

    def store(self):
        pass

    def render(self, **kwargs):
        """
        Prints gamefield as matrix.
        :return: A numpy array containing the gamefield.
        """
        return print(self.game.gamefield)

    def close(self):
        pass