from gym_honeycomb.envs.honeycomb_env import HoneyCombEnv


class HoneyCombMinorityEnv(HoneyCombEnv):
    """ The environment for the informed minority of the HoneyComb Game.
    In this game, one of the six reward fields returns a higher reward!
    """

    def __init__(self):
        super().__init__()
        self.game.PAYOFF_SPECIAL = [(9, 3)]

    # change reward function
    @property
    def reward_payoff_field(self):
        """
        The six payoff fields yield a reward of 1, if the agent ends the game on the field.
        The payoff is multiplied by the number of agents on the same reward field. On one
        special payoff field, the agents receive a special reward of 2.
        :return: A list of individual agent reward and the summarized rewards
        """
        reward_list = []
        for agent in range(self.game.agents):
            payoff = 0
            num = 0
            if self.game.agent_positions[agent] in self.game.PAYOFF_FIELD:
                if self.game.agent_positions[agent] in self.game.PAYOFF_SPECIAL:
                    payoff = 2
                else:
                    payoff = 1

                for pos in range(self.game.agents):
                    if self.game.agent_positions[agent] == self.game.agent_positions[pos]:
                        num += 1
                
            reward = num * payoff
            reward_list.append(reward)
        return reward_list, sum(reward_list)