import copy
from gym_honeycomb.envs.honeycomb_env import HoneyCombEnv


class HoneyCombSimulationEnv(HoneyCombEnv):
    """ Simulation Environment with different Rewards.
    """

    def __init__(self, max_turns=600):
        super().__init__()

    # change reward function
    @property
    def reward_payoff_field(self):
        """
        The six payoff fields yield a reward of 0.5, if the agent ends the game on the field.
        The payoff is multiplied by the number of agents on the same reward field. On one
        special payoff field, the informed agents receive a special reward of 1.
        :return: A list of individual agent reward and the summarized rewards
        """
        reward_list = []
        for agent in range(self.game.agents):
            payoff = 0
            num = 0
            if self.game.agent_positions[agent] in self.game.PAYOFF_FIELD:
                if (self.game.agent_positions[agent] in self.game.PAYOFF_SPECIAL) and ((agent == 8) or (agent == 9)):
                    payoff = 1
                else:
                    payoff = 0.5

                for pos in range(self.game.agents):
                    if self.game.agent_positions[agent] == self.game.agent_positions[pos]:
                        num += 1

            reward = num * payoff
            reward_list.append(reward)
        return reward_list, sum(reward_list)

    def make_next_agent(self, valid_move=None):
        """
        Define the next agent.
        :param valid_move: bool
        :return: -
        """
        self.game.current_agent, self.done = self.game.next_agent()

    def store(self):
        self.dataframe_check = self.make_check_df
        self.dataframe_move = self.make_df_move
        self.dataframe_eval = self.make_df_eval

    def reset(self):
        """
        At the end of an episode call reset_game to initialize a new gamefield.
        :return: A numpy array of current state.
        """
        # Initialize a new gamefield
        self.game.reset_game()

        # Initialize lag obs
        self.lag_observations = copy.copy(self.game.agent_positions)

        return self.get_observations