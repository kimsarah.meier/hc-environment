# Simulation of the HoneyComb Experiment
> A custom gym environment for the HoneyComb Experiment. The environment can be used to train agents with RL algorithms of different implementations.
> Additionally functions for simulation with trained agents and visualization of results are provided.

<img src="data/emptyGamefield.png" align="right" width="40%"/>
<hr>

# Table of Contents
* [Environments](#environments)
* [Installation](#installation)
* [Team Members](#team-members)

# <a name="environments"></a>Environment
Inlcuded are 
* "honeycomb-v0" for the uninformed majority of agents.
* "honeycomb-minority-v0" for the informed minority of agents.
* "honeycomb-sim-v0" for simulation using one or ten trained agents simultaneously.

# <a name="installation"></a>Installation
```bash
! git clone https://gitlab.gwdg.de/kimsarah.meier/hc-environment.git
! cd hc-environment
! pip install -e hc-environment
```

# <a name="team-members"></a>Team Members
* Kim Sarah Meier <kimsarah.meier@stud.uni-geottingen.de>
* Henrike Meyer <henrike.meyer@stud.uni-geottingen.de>
